import random
import os
import ascii_art

class random_br:
    
    lineups = {}
    nations = []
    brs = []
    planes = []
    
    # clear = lambda: os.system('cls')   
    def __init__(self):
        self.load_lineups_from_file('lineups.txt')
        self.nations = self.load_from_file('nations.txt')
        self.brs = self.load_from_file('brs.txt')
        self.planes = self.load_from_file('planes.txt')
        self.print_welcome()
      
    def print_banner(func):
        def inner(self, *args, **kwargs):
            os.system('cls')     
            # print(ascii_art.tank1)
            ascii = open('ascii.txt')
            print(ascii.read())
            func(self, *args, **kwargs)
        return inner
        
    # @print_banner
    def print_welcome(self):
        ascii = open('ascii.txt')
        print(ascii.read())
        print('   Welcome to RANDOM BR!')
        print("   Loaded default files")
        print("   Press any key to continue ...") 
        input()
        self.print_menu()
        
    # @print_banner 
    def print_menu(self):
        os.system('cls')  
        print('   Choose: (B)R / (N)ation / (L)ineup / (P)lane / (Q)uit / (C)lean / Enter to repeat')
        user_input = input().lower()
        self.print_result(user_input)
        
          
    def print_result(self, user_input):
        match user_input:
            case 'br' | 'b':
                self.choose_random_br()
            case 'nation' | 'n':
                self.choose_random_nation()
            case 'lineup' | 'l':
                self.choose_random_lineup()
            case 'plane' | 'p':
                self.choose_random_plane()
            case 'q':
                os.system('cls')
                exit()
            case _:
                self.print_menu()
        user_input2 = input().lower()
        if user_input2 == '':
            self.print_result(user_input)
        else:
            self.print_result(user_input2)
        
    def read_lineup(self, line):
        lineup = {}
        brs = []
        
        values = line.strip('\n').split(' ')
        nation = values.pop(0)
        if nation == '#':
            return {}
        for value in values:
            brs.append(value)
        lineup[nation] = brs
        
        return lineup
    
    
    def load_lineups_from_file(self, file_name):
        file = open(file_name, 'r')
        for line in file:
            self.lineups = self.lineups | self.read_lineup(line)
        # print(self.lineups)
        
    def load_from_file(self, file_name):
        tem = []
        file = open(file_name, 'r')
        for line in file:
            if line[0] == "#":
                continue
            tem.append(line.strip('\n'))
        return tem
        # print(self.nations)
        
    def choose_random_lineup(self):
        nation = random.choice(list(self.lineups.keys()))
        br = random.choice(self.lineups[nation])
        print("   Lineup:  {} {}".format(nation, br))
        
    def choose_random_plane(self):
        print("   PLANE:  " + random.choice(self.planes))  
        
    def choose_random_nation(self):
        print("   NATION:  " + random.choice(self.nations))
        
    def choose_random_br(self):
        print("   BR:  " + random.choice(self.brs))
     
        
random_br_1 = random_br()